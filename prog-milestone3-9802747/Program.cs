﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9802747
{
    class Program
    {
         static void Main(string[] args)
        {
            int startmenu;
            int i;
            int x;
            string pizzatype;
            string currency;

            var type = new List<string> { "Hawaiian  ", "MeatLover ", "Supreme   ", "Vegetarian" };


            List<Tuple<string, double>> drinks = new List<Tuple<string, double>>();
            drinks.Add(Tuple.Create("Coke        ", 5.00));
            drinks.Add(Tuple.Create("Beer        ", 7.00));
            drinks.Add(Tuple.Create("Wine        ", 9.00));

            List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
            pizza.Add(Tuple.Create("Small Pizza ", 5.00));
            pizza.Add(Tuple.Create("Medium Pizza", 10.00));
            pizza.Add(Tuple.Create("Large Pizza ", 15.00));

            

        Menustart:
            Console.Clear();
        
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine("Welcome to Pizza Palace, may I take a name for your order?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. Later");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            if (int.TryParse(Console.ReadLine(), out startmenu))
            {
                switch (startmenu)
                {
                    case 1:
                        Customer.GetCustDetails();

                        break;

                    case 2:
                        goto Orderstart;
                        break;

                    default:
                        {
                            Console.WriteLine("Invalid input: Press any key to try again.");
                            Console.ReadKey();
                            goto Menustart;
                            break;
                        }

                }
            }
            else
            {
                Console.WriteLine("Invalid input: Press any key to try again.");
                Console.ReadKey();
                goto Menustart;
            }
        Orderstart:


            Console.Clear();
            Order.ShowOrder();
            Console.WriteLine("Would you like to order Pizza or drink?");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Drink");
            Console.WriteLine("3. Edit Order");
            Console.WriteLine("4. Change Customer Details");
            Console.WriteLine("5. Finalise");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:
                    Pizzastart:
                        Console.Clear();

                        Console.WriteLine("Current Order:");
                        Order.ShowOrder();
                        for (i = 0; i < type.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {type[i]}");
                        }
                        Console.WriteLine($"{i + 1}. Return to menu");
                        Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto Orderstart;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Invalid input: Press any key to try again.");
                                Console.ReadKey();
                                goto Pizzastart;
                            }
                            else
                            {
                                pizzatype = type[x-1];
                                Console.Clear();
                                Order.ShowOrder();
                                Console.WriteLine($"Current Pizza Type: {pizzatype}");
                                for (i = 0; i < pizza.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {pizza[i].Item1}");
                                }
                                Console.WriteLine($"{i + 1}. Return to menu");
                                Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto Orderstart;
                                    }
                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine("Invalid input: Press any key to try again.");
                                        Console.ReadKey();
                                        goto Pizzastart;
                                    }
                                    else
                                    {
                                        Order.order.Add(Tuple.Create(pizzatype, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                    }
                                    goto Pizzastart;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input: Press any key to try again.");
                                    Console.ReadKey();
                                    goto Pizzastart;
                                }
                            }
                            goto Pizzastart;
                        }
                        else
                        {
                            Console.WriteLine("Invalid input: Press any key to try again.");
                            Console.ReadKey();
                            goto Pizzastart;
                        }

                        
                    case 2:
                    Drinkstart:

                        Console.Clear();

                        Order.ShowOrder();
                        for (i = 0; i < drinks.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {drinks[i].Item1}");
                        }
                        Console.WriteLine($"{i + 1}. Return to menu");
                        Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto Orderstart;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Invalid input: Press any key to try again.");
                                Console.ReadKey();
                                goto Drinkstart;
                            }
                            else
                            {
                                Order.order.Add(Tuple.Create("Drink     ",drinks[x - 1].Item1, drinks[x - 1].Item2));
                            }
                            goto Drinkstart;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid input: Press any key to try again.");
                            Console.ReadKey();
                            goto Drinkstart;
                        }

                    case 3:
                    Editstart:
                        Console.Clear();
                        Order.ShowOrder();
                        Console.WriteLine("1. Remove Item");
                        Console.WriteLine("2. Continue");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            switch (x)
                            {
                                case 1:
                                    Deletestart:
                                    Console.Clear();

                                    Console.WriteLine("Current Order:");
                                    
                                    for (i = 0; i < Order.order.Count; i++)
                                    {
                                        currency = String.Format("{0:C}", Order.order[i].Item3);
                                        Console.WriteLine($"{i + 1}      {Order.order[i].Item1}      {Order.order[i].Item2}      {currency}");
                                    }
                                        Console.WriteLine($"{i + 1}      Return to Menu");
                                        Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
                                        
                                        Console.WriteLine("Which entry to remove?");
                                    if (int.TryParse(Console.ReadLine(), out x))
                                    {
                                        if (x - 1 < Order.order.Count)
                                        {
                                            Order.order.Remove(Order.order[x - 1]);
                                            goto Deletestart;
                                        }
                                        else if (x == i + 1)
                                        {
                                            goto Editstart;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Invalid input: Press any key to try again.");
                                            Console.ReadKey();
                                            goto Deletestart;

                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Invalid input: Press any key to try again.");
                                        Console.ReadKey();
                                        goto Deletestart;
                                        break;
                                    }
                                case 2:
                                    goto Orderstart;
                                    break;

                                default:
                                    Console.WriteLine("Invalid input: Press any key to try again.");
                                    Console.ReadKey();
                                    goto Editstart;
                                    break;

                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input: Press any key to try again.");
                            Console.ReadKey();
                            goto Editstart;
                            break;
                        }
                        break;

                    case 4:
                        Customer.GetCustDetails();
                        goto Orderstart;

                    case 5:
                    Finalise:
                        if (Customer.phone == 0)
                        {
                            Customer.GetCustDetails();
                            goto Finalise;
                        }
                        else
                        {

                            Console.Clear();
                            Order.ShowOrder();
                            Console.WriteLine("Is this order Correct?");
                            Console.WriteLine("1. Yes");
                            Console.WriteLine("2. No");
                            if (int.TryParse(Console.ReadLine(), out x))
                            {
                                switch (x)
                                {
                                    case 1:
                                        Environment.Exit(0);
                                        break;

                                    case 2:
                                        goto Orderstart;
                                        break;
                                }
                            }




                        }
                        break;

                    default:
                        Console.WriteLine("Invalid input: Press any key to try again.");
                        Console.Read();
                        goto Orderstart;




                }
            }
            else
            {
                Console.WriteLine("Invalid input: Press any key to try again.");
                Console.Read();
                goto Orderstart;
            }

        }
    }



    public static class Customer
    {
        public static string name;
        public static int phone;
        public static string displayphone;
        
        
        public static void GetCustDetails()
        {
            int x;
        Start:
            Console.Clear();
            Console.WriteLine("What is the Customers Name?");
            name = Console.ReadLine();

            Console.WriteLine("What is the Customers Contact Number? (no spaces)");
            if (int.TryParse(Console.ReadLine(), out phone))
            {
                displayphone = ($"0{phone}");
                Console.WriteLine($"The name is {name}");
                Console.WriteLine($"The phone is {displayphone}");
                Console.WriteLine("");
                Console.WriteLine("Are these details correct?");
                Console.WriteLine("1. Yes");
                Console.WriteLine("2. No");
                
                if(int.TryParse(Console.ReadLine(), out x))
                {
                    switch (x)
                    {
                        case 1:
                            break;

                        default:
                            goto Start;
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input: Please use numbers only. Press any key to try again.");
                    Console.ReadKey();
                    goto Start;
                }
               
            }
            else
            {
                Console.WriteLine("Invalid input: Please use numbers only. Press any key to try again.");
                Console.ReadKey();
                goto Start;
            }


        }
            
    }

    public static class Order
    {
        
        public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

        public static void ShowOrder()
        {
            int i;
            double total;
            string currency; 

            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine($"Name: {Customer.name}");
            Console.WriteLine($"Phone: {Customer.displayphone}");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine("Current Order:");
            total = 0;
            for (i = 0; i < order.Count; i++)
            {
                currency = String.Format("{0:C}", order[i].Item3);
                Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {currency}");
                total = total + order[i].Item3;
            }
            currency = String.Format("{0:C}", total);
            Console.WriteLine($"                TOTAL:            {currency}");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
        }

    }




}
   